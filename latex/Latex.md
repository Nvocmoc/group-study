[Learn by yourself](https://www.sharelatex.com/learn/Main_Page)

### 1. TexLive

- [TeX Live](https://en.wikipedia.org/wiki/TeX_Live) is a software distribution for TeX typesetting system (all platforms)
- Another popular distribution is MiKTex (Windows)
- Commands, library manager, libraries

### 2. TexStudio

- An advanced editor, providing shortcuts and intellisense, not part of Tex system
- Save the following as test.tex, then **build and view**

~~~~latex
\documentclass[12pt]{article}
\begin{document}
% a cute comment
\TeX{} is good at typesetting words like `fjord', `efficiency',
and `fiasco'. It is also good at typesetting math like,
$a^2 + b^2 = c^2$.
\end{document}
~~~~
- **Options** ->  **Configure TeXstudio** to learn common useful commands

  ​

### 3. Tex commands

- *latex/pdflatex/lualatex/xelatex* typesetting tools, many subtle differences [Info](https://www.sharelatex.com/blog/2012/12/01/the-tex-family-tree-latex-pdftex-xelatex-luatex-context.html)
- ​Usually compile .tex file to .dvi format then to .pdf or .ps files (*latex/tex* first then *dvipdfm*)
- With *pdflatex -pdf test.tex* or *latexmk -pdf test.tex*, a tool chain is all set up

### 4. Tex grammar
#### Control commands
- **%** followed by comments 
- **\** followed by control sequeces, then **[]** for optional arguments, **{}** for necessary arguments
- Control sequence **begin** always pair with **end**, they create a environment in the middle, both followed by the environment name
- Only content inside *document* environment would affect final document
- Between **\begin{document}** and **\documentclass{article}** was intro part, setup page size, title format, page foot style, etc.
- A new line by **\\\\** or two empty blank lines

#### Articles
- Try the following to organize the structure.
~~~~latex
\documentclass{article}
\title{Hello，world!}
\author{Human}
\date{\today}
\begin{document}
\maketitle
\tableofcontents
\section{Hello Mammals}
We are mammals.
\subsection{Hello Dogs}
Puppies are mammals.
\subsubsection{Hello Labrador}
\subsubsection{Hello Golden Retriever}
\subsection{Hello Cats}
Cats are also mammals.
\end{document}
~~~~

#### Math equations
- Need extra package **\usepackage{amsmath}**
- [Online Info](https://reu.dimacs.rutgers.edu/Symbols.pdf)
- Two modes, inline or display mode
   - inline: **$...$** or **\\(...\\)**
   - display: **\\[...\\]** or *equation* environment if index desired
   - **\begin{equation\*} ... \end{equation\*}** if no index desired
- Sub/super script: **a_{12}** and **a^{\dagger}**
- **\sqrt{2}**, **\frac{1}{2}** ...
- **\dots**, **\cdots**, **\vdots** ...

~~~~latex
\documentclass{article}
\usepackage{amsmath}
\begin{document}

\[ \Biggl(\biggl(\Bigl(\bigl((x)\bigr)\Bigr)\biggr)\Biggr) \]
\[ \Biggl[\biggl[\Bigl[\bigl[[x]\bigr]\Bigr]\biggr]\Biggr] \]
\[ \Biggl \{\biggl \{\Bigl \{\bigl \{\{x\}\bigr \}\Bigr \}\biggr \}\Biggr\} \]
\[ \Biggl\langle\biggl\langle\Bigl\langle\bigl\langle\langle x
\rangle\bigr\rangle\Bigr\rangle\biggr\rangle\Biggr\rangle \]
\[ \Biggl\lvert\biggl\lvert\Bigl\lvert\bigl\lvert\lvert x
\rvert\bigr\rvert\Bigr\rvert\biggr\rvert\Biggr\rvert \]
\[ \Biggl\lVert\biggl\lVert\Bigl\lVert\bigl\lVert\lVert x
\rVert\bigr\rVert\Bigr\rVert\biggr\rVert\Biggr\rVert \]

\begin{equation}
\begin{pmatrix} a&b\\c&d \end{pmatrix} \quad
\begin{bmatrix} a&b\\c&d \end{bmatrix} \quad
\begin{Bmatrix} a&b\\c&d \end{Bmatrix} \quad
\begin{vmatrix} a&b\\c&d \end{vmatrix} \quad
\begin{Vmatrix} a&b\\c&d \end{Vmatrix} 
\end{equation}
\end{document}
~~~~


#### Figures
- *pic/path* should be relative path of figure to work directory
- "*htbp*" for "here, top, bottom, float page", position for the float figure
- *label* can be further refered to by **\ref{fig:blah}** in article

~~~~latex
\documentclass{article}
\usepackage{graphicx}
\begin{document}
\begin{figure}[htbp]
% width would be 80% of page width
\includegraphics[width=.8\textwidth]{pic/path.jpg}
\caption{Blahblah is blahblah}
\label{fig:blah}
\end{figure}  
\end{document}
~~~~

#### Tables
- *tabluar* environment
~~~~latex
\documentclass{article}
\begin{document}
\begin{tabular}{|l|c|r|}
	\hline
	OS & Release& Editor\\
	\hline
	Windows & MikTeX & TexMakerX \\
	\hline
	Unix/Linux & teTeX & Kile \\
	\hline
	Mac OS & MacTeX & TeXShop \\
	\hline
	Universal & TeX Live & TeXworks \\
	\hline
\end{tabular}
\end{document}
~~~~

#### Miscellaneous
- *geometry* for page edge distance
~~~~latex
\usepackage{geometry}
\geometry{papersize={20cm,15cm}}
\geometry{left=1cm,right=2cm,top=3cm,bottom=4cm}
~~~~
- *setspace* for line distance
~~~~latex
\usepackage{setspace}
\onehalfspacing
% line space is 1.5 times font size
~~~~

### 5. Reference management
- Prepare .bib file, content copied from paper citation
- First line is a mar name can be further cited by **\cite{THC1}**
~~~~latex
@article{THC1,
  author = {Hohenstein, E.~G. and Parrish, R.~M. and Mart\'{i}nez, T.~J.},
  title = {Tensor hypercontraction density fitting. I. Quartic scaling second- and third-order
    M\o\/ller-Plesset perturbation theory},
  year = {2012},
  journal = {J. Chem. Phys.},
  volume = {137},
  number = {4},
  pages = {044103}
}
~~~~
- Add reference part to article
~~~~latex
\bibliographystyle{unsrt}
\bibliography{references}
~~~~

### 6. tlmgr 

- Try *tlmgr -gui*
- A library management tool, download and update from certain repository