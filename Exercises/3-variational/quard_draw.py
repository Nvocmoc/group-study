import numpy as np
import matplotlib.pyplot as pl
from mpl_toolkits.mplot3d import axes3d

A = np.random.rand(2, 2) + np.eye(2)
B = np.random.rand(2, 1)
A = A + A.T
a, b = A[0]
c, d = A[1]
b1, b2 = B


space = 100
num = 2
x0 = np.linspace(-1 * num, num, space)
y0 = np.linspace(-1 * num, num, space)
e, v = np.linalg.eig(A)
k = v[1, :] / v[0, :]
k0 = k[0] * x0
k1 = k[1] * x0
x, y = np.meshgrid(x0, y0)
# x=np.append(x0,x0)
# y=np.append(k0,k1)

temp = a * x * x + (b + c) * x * y + d * y * y
quard = temp + b1 * x + b2 * y
norm = x * x + y * y
rayleigh = temp / norm

# denominator=norm*norm*norm/2

derivative1 = (a * x + b * y - rayleigh * x)
derivative2 = (c * x + d * y - rayleigh * y)


filt1 = np.abs(k0) < num
filt2 = np.abs(k1) < num
filt = filt1 if np.sum(filt1) < np.sum(filt2) else filt2
k0 = k0[filt]
x0 = x0[filt]
k1 = k1[filt]


fig1 = pl.figure('Ax=b')
ax1 = fig1.gca(projection='3d')
ax1.plot_surface(x, y, quard, color='y')


fig2 = pl.figure('Ax=kx')
ax2 = fig2.gca(projection='3d')
ax2.plot_surface(x, y, rayleigh, color='m')

ax2.plot(x0, k0, color='g')
ax2.plot(x0, k1, color='g')

# fig3 = pl.figure("derivative")
# ax3 = fig3.add_axes([0, 0.1, 0.5, 0.8], projection='3d')
# ax3.plot_surface(x, y, derivative1, color='y')
# ax4 = fig3.add_axes([0.5, 0.1, 0.5, 0.8], projection='3d')
# ax4.plot_surface(x, y, derivative2, color='y')
# ax3.plot(x0, k0, color='g')
# ax3.plot(x0, k1, color='g')
# ax4.plot(x0, k0, color='g')
# ax4.plot(x0, k1, color='g')

# fig4 = pl.figure('quiver')
# ax = fig4.gca()
# ax.quiver(x, y, x + derivative1, y + derivative2)
# ax.plot(x0, k0, color='g')
# ax.plot(x0, k1, color='g')
# x = x0[np.abs(x0) < 1]
# y = np.sqrt(1 - x * x)
# ax.plot(x, y, color='y')
# ax.plot(x, -y, color='y')


pl.show()
