print ["".join([chr((ord(x)-ord("a")+i)%26+ord("a")) for x in "totoro"]) for i in range(26)] 

# Or in a function-like way, using lambda expression:

print (lambda input: ["".join([chr((ord(x)-ord("a")+i)%26+ord("a")) for x in input]) for i in range(26)])("totoro")