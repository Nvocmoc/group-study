

def Caesar_cipher(string = "totoro"):
	"""
	Definition: https://en.wikipedia.org/wiki/Caesar_cipher
	Function input: a string
	Function return: a list of 26 Caesar cipher strings
	Hints: you need to know these: \ 
	       function ord(), chr() and "".join(string-list)
	       ASCII representation of [a-z]
	"""
	string = string.lower()
	li = []

	# Write your code here, fill li with all 26 Caesar ciphers

	return li

if __name__ == '__main__':
	li = Caesar_cipher()
	print li
	""" 
	Expecting ['totoro', 'upupsp', 'vqvqtq', 'wrwrur', 'xsxsvs', 'ytytwt', 'zuzuxu', 'avavyv', 'bwbwzw', 'cxcxax', 'dydyby', 'ezezcz', 'fafada', 'gbgbeb', 'hchcfc', 'ididgd', 'jejehe', 'kfkfif', 'lglgjg', 'mhmhkh', 'ninili', 'ojojmj', 'pkpknk', 'qlqlol', 'rmrmpm', 'snsnqn']
	If you have finished, try to use list comprehension to print all 26 strings of 'totoro' in one line!
	"""
