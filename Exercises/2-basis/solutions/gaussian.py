import numpy as np
import math
import matplotlib.pyplot as plt


class Slater(object):
    def __init__(self, a, coor):
        # amplitude, atom center
        self.a = a
        self.coor = np.array(coor)
        self.norm = (self.a**3 / np.pi)**0.5

    def evaluate(self, coor):
        coor = np.array(coor)
        return self.norm * np.exp(-1.0 * self.a * np.linalg.norm(self.coor - coor))


class Gaussian(object):
    def __init__(self, a, coor):
        # amplitude, atom center
        self.a = a
        self.coor = np.array(coor)
        self.norm = np.power(np.pi / (2 * self.a), -3.0 / 4.0)

    def evaluate(self, coor):
        coor = np.array(coor)
        return self.norm * np.exp(-1.0 * self.a * (np.linalg.norm(self.coor - coor)**2))

    def __mul__(self, other):
        new_a = self.a + other.a
        new_coor = (self.a * self.coors + other.a * other.coors) / new_a
        adjusted = self.a * other.a / new_a
        K_AB = (2 * adjusted / np.pi)**(3.0 / 4.0) * \
            np.exp(-1.0 * adjusted * (np.norm(self.coor - other.coor)**2))
        return K_AB, Gaussian(new_a, new_coor)

    def int_S(self, other):
        coor = self.coor - other.coor
        a = np.exp(-(self.a * other.a / (self.a + other.a))
                   * np.dot(coor, coor))
        b = np.power(np.pi / (self.a + other.a), 1.5)
        return self.norm * other.norm * a * b

    def int_T(self, other):
        a = (self.a * other.a) / (self.a + other.a)
        coor = self.coor - other.coor
        norm = np.dot(coor, coor)
        result = self.norm * other.norm * \
            np.power(np.pi / (self.a + other.a), 1.5)
        result *= np.exp(-1.0 * a * norm)
        result *= (-2.0 * a**2 * norm + 3.0 * a)
        return result

    def int_V(self, other, mol):
        temp = np.pi / (self.a + other.a)
        result = -1.0 * self.norm * other.norm * temp
        norm = np.dot(self.coor - other.coor, self.coor - other.coor)
        result *= np.exp(-1.0 * (self.a * other.a / (self.a + other.a)) * norm)
        output = 0.0
        coor = (self.a * self.coor + other.a * other.coor) / (self.a + other.a)
        for i, z in enumerate(mol.Zs):
            if np.allclose(mol.coors[i], coor):
                output += result * z * 2
            else:
                rac = np.linalg.norm(coor - mol.coors[i])
                output += result * z * \
                    np.sqrt(temp) * \
                    math.erf(np.sqrt(self.a + other.a) * rac) / rac
        return output


def eri(basis1, basis2, basis3, basis4):
    p = len(basis1)
    q = len(basis2)
    r = len(basis3)
    s = len(basis4)
    eri_J = np.zeros((p * q, r * s))

    left = []
    r_left = []
    sum_left = []

    right = []
    r_right = []
    sum_right = []

    for i in xrange(p):
        b1 = basis1[i]
        n1, a1, coor1 = b1.norm, b1.a, b1.coor
        for j in xrange(q):
            b2 = basis2[j]
            n2, a2, coor2 = b2.norm, b2.a, b2.coor
            r1 = np.linalg.norm(coor2 - coor1)
            sum_a = a1 + a2
            scalar = n1 * n2 * ((np.pi / sum_a)**1.5) * \
                np.exp(-1.0 * a1 * a2 / sum_a * r1**2)
            left.append(scalar)
            r_left.append((a1 * coor1 + a2 * coor2) / sum_a)
            sum_left.append(sum_a)

    for k in xrange(r):
        b3 = basis3[k]
        n3, a3, coor3 = b3.norm, b3.a, b3.coor
        for l in xrange(s):
            b4 = basis4[l]
            n4, a4, coor4 = b4.norm, b4.a, b4.coor
            r2 = np.linalg.norm(coor4 - coor3)
            sum_a = a3 + a4
            scalar = n3 * n4 * ((np.pi / sum_a)**1.5) * \
                np.exp(-1.0 * a3 * a4 / sum_a * r2**2)
            right.append(scalar)
            r_right.append((a3 * coor3 + a4 * coor4) / sum_a)
            sum_right.append(sum_a)

    for i in xrange(q * p):
        for j in xrange(r * s):
            rou = (sum_right[j] * sum_left[i]) / (sum_right[j] + sum_left[i])
            if np.allclose(r_left[i], r_right[j]):
                # print rou, left[i],right[j]
                eri_J[i, j] = left[i] * right[j] * 2 * (rou / np.pi)**0.5
            else:
                rcd = np.linalg.norm(r_left[i] - r_right[j])
                eri_J[i, j] = left[i] * right[j] * \
                    math.erf(rou**0.5 * rcd) * rcd
    return eri_J


class Mol(object):
    def __init__(self, coors, Zs):
        self.coors = coors
        self.Zs = Zs

    def get_nuc_energy(self):
        E = 0.0
        for i1, m1 in enumerate(self.coors):
            for i2, m2 in enumerate(self.coors):
                if i2 >= i1:
                    continue
                E += self.Zs[i1] * self.Zs[i2] / np.linalg.norm(m1 - m2)
        return E


def one_elec(mol, basis_name, basis_set_up={}):
    gaussians = get_basis(mol, basis_name, basis_set_up)

    dim = len(gaussians)
    T = np.zeros((dim, dim))
    V = np.zeros((dim, dim))
    S = np.zeros((dim, dim))
    for i in xrange(dim):
        for j in xrange(i + 1):
            V[i][j] = gaussians[i].int_V(gaussians[j], mol)
            T[i][j] = gaussians[i].int_T(gaussians[j])
            S[i][j] = gaussians[i].int_S(gaussians[j])
            V[j][i] = V[i][j]
            T[j][i] = T[i][j]
            S[j][i] = S[i][j]

    H = T + V
    eig, vector = np.linalg.eigh(S)
    X = np.dot(np.dot(vector, eig**(-0.5) * np.eye(dim)), vector.T)

    H_trans = np.dot(np.dot(X.T, H), X)
    E, C_trans = np.linalg.eigh(H_trans)
    E = E + mol.get_nuc_energy()
    return E


def even_temper(mol, set_up={}):
    dim = set_up.get("dim", 9)
    a = set_up.get("a", 0.5)
    beta = set_up.get("beta", 1.5)
    const = (dim - 1) / 2
    alphas = [a * np.power(beta, p - const) for p in xrange(dim)]
    gaussians = []
    for coor in mol.coors:
        gaussians.extend([Gaussian(a, coor) for a in alphas])
    return gaussians


def sto_1g(mol, set_up={}):
    return [Gaussian(0.435052, coor) for coor in mol.coors]
    # return [Gaussian(0.41661272, coor) for coor in mol.coors] # eta = 1.24**2 *0.270950


def get_basis(mol, basis_name, set_up={}):
    basis_set = {"even_temper": even_temper,
                 "sto_1g": sto_1g,
                 }
    basis_name = basis_name.replace(" ", "").lower()
    basis_function = basis_set.get(basis_name, None)
    if not basis_function:
        raise NotImplemented("Not conatining this basis set!")
    else:
        return basis_function(mol, set_up)


if __name__ == '__main__':
    # space = np.linspace(-1,1,100)
    # alphas = [0.5,1,2,4,8]
    # gaussians = [Gaussian(a, [0]) for a in alphas]
    # slaters = [Slater(a, [0]) for a in alphas]
    # gs = [[g.evaluate(r) for r in space] for g in gaussians]
    # ss = [[s.evaluate(r) for r in space] for s in slaters]

    # f, (ax1, ax2) = plt.subplots(1, 2)
    # for i in xrange(len(alphas)):
    # 	ax1.plot(space,gs[i],label="G-"+str(alphas[i]))
    # 	ax2.plot(space,ss[i],label="S-"+str(alphas[i]))
    # ax1.legend()
    # ax2.legend()
    # plt.savefig("orbitals.png")
    # plt.clf()

    mol = Mol(np.array([[0, 0, 0]]), [1])
    print one_elec(mol, "even_temper", {"dim": 9})

    R = np.linspace(0.2, 10, 80)
    mols = [Mol(np.array([[0, 0, 0], [r, 0, 0]]), [1, 1]) for r in R]
    Es = [one_elec(mol, "even_temper", {"dim": 9})[:2] for mol in mols]
    E0, E1 = zip(*Es)

    plt.axis([0.2, 10., -0.65, +0.25])
    plt.plot(R, E0, label="E0")
    plt.plot(R, E1, label="E1")
    plt.legend()
    plt.savefig("H2+.png")
