## Welcome to Hohenstein Group!

- Hohenstein group is a computational chemistry group in CCNY, dedicated to developing and applying wavefunction-based electronic structure methods to a variety of challenging chemical problems.

- This is an exercise code repository for starters in computational chemistry to enhance code skills and have better understanding of quantum mechanics.

- Some exercises will be provided, please create your own branch and submit your solutions.

- Always talk with others when having a problem.

- Some useful complement materials can be found in [wiki](https://bitbucket.org/Nvocmoc/group-study/wiki/Home).
## Contact
- Questions about the repository should be directed to Yao (dlesces@gmail.com) or Max (mmathless@gradcenter.cuny.edu).

### *Have fun!*