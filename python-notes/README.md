### Outline

- Class 1: basic shell commands, basic python
- Class 2: function, library, Boolean, int, float, list, tuple
- Class 3: list, memory, tuple, loops
- Class 4: set, dict, string, unicode, 
- Class 5: list comprehension, class, python objects

These tutorials are written accompanying some oral lectures, so they may not be very specific and clear. If you have any questions, please contact Yao.

#### *Have fun!* 