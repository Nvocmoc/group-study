# The Beginning 

## 1. Using the command line/terminal 

- Terminal: How man overpower machine
- Ubuntu (linux): window + Alt + T
- Windows: search "cmd" or shift + right-click in folder -> Open command window here

## 2. Basic commands

- **pwd** show the path of current working directory 

- **ls *dirname* **  display all files/folders under *dirname* directory, by default the current directory

- **cd *dirname* **  change the working place to *dirname*

- **man *command* **  inquire usage of *command*, search by "/search"

- **touch**, **mkdir**, **rm**, **mv**, **cp** 

- **echo**, **cat**, **which**

- **grep**, **sed**, **tail**, **top**, **free**, **nvidia-smi**

- **pip**, **ipython**

Q: How do computer know where to run the command?

Hint: Try **echo $PATH**, system environment

## 3. Basic Python

#### First python:

```python
print 'Hello World'
text = 'Hello World'
print text 

print 1+2
num = 1+2
print 'The num is: ',num
```

#### In interacting mode:

-  Input line by line
-  **help()** very useful, inquire usage of function, must learn
-  Function **dir()** similar, only names


### In run-script mode

- Store in a file named *'fileName.py'*, then in the same directory call **python fileName.py**
- Tab trick to auto-fill file name
- Introduction of $vim$, $Emacs$ and $nano$, $Sublime\ Text$ suggested
- Interpreter goes through line by line to operate



#### Variables

- Corresponding to a block of memory, used to control memory
- Variable types: determine how variable is stored, what can we do with it, dynamically determined in python
- Nomenclature: *variable_name* or *variableName*, case-sensitive


#### Functions

- Several lines encapsulated together
- Affiliation indicated by indentation (reason why Python is simple and elegant)
- "Life is short, I use Python", comparing with jokes and wars in C++
- Consist of: function name, arguments, comments(optional), content, return(optional)

~~~python
def funcName(arg1, arg2):
  """
  This comment, invisiable to intepretor, is to remind you today is the last day of 2016
  """
  print "NY ROLLS"
  result =arg1+arg2 # short comments start with a #
  return result
~~~

- Call function:

~~~python
funcName(1,2)
output = funcName(2,3)
print output
~~~

- Libraries: functions used a lot, provided by language or community contributors

- [Built in libraries](https://docs.python.org/2/library/) and [Built in functions](https://docs.python.org/2/library/functions.html)

~~~python
import os
os.listdir()
        
import sys 
sys.path
~~~

## 4. Rest time

- Zen of python: **import this**
- He is flying: **import antigravity**


