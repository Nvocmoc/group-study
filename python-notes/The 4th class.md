#  The Forth class

## Built-in types

### 1. Boolean
### 2. Numbers
### 3. List
### 4. Tuple
### 5. Set
- Like mathematical sets: not ordered, no repeated items
- Can only contain immutable objects
- Practices:

~~~~python
a_set = {0,1,2,3,4}
a_set = set()                          # type({}) is "dict"
li = range(5)+[False, 1.0, "True",10]
a_set = set(li)                        # set(Iterable), order changed
print li
print a_set,"\n"                       # False treated as 0
"""
Recall that remove(1.0) is same as remove(1), let's check source code
In Python2, True is a Built-in Constant, "True=2" is legal
This is not the case in many languages, even illegal in Python3
"""

# learn by help(set)
a_set.add((1,))
a_set.add(10)                          # nothing happens, no error
print a_set,"\n"
# a_set.add([])                        # need immutable to be hashed
# a_set.add((1,[2,3]))                 # tuple may not be good enough

rang = range(3,10)
print rang
a_set.update(rang)                     # can update list, set, tuple
print a_set,"\n"                       # duplicated item removed

a_set.discard(1)
a_set.discard(1)                       # won't give error
print a_set
a_set.remove(10)                       # use twice would give error
print a_set,"\n"

print a_set.pop()                      # help(set.pop), ditto in list
for i in a_set: print i                # "random" output

b_set = {1, 2, 3, 5, 6, 8, 9, 12, 15, 17, 18, 21}
print a_set.union(b_set)               # Q: difference with update?
print a_set.intersection(b_set)
print a_set.difference(b_set)
print a_set.symmetric_difference(b_set)

print {1,2,3}.issubset({1,2,3,4})
print {1,2,3,4}.issuperset({1,2,3})

if not set() and {False}: print "Empty set treated as false"
~~~~

  ​

### 6. Dict
- Dictionary, called "map" in may languages, key-value pairs 
- No duplicated keys, only hash-able items can be key, not ordered
- Many operations operated on keys
- Dict, Set are both generally faster than List, [Time Complexity](https://wiki.python.org/moin/TimeComplexity)
- Practices:

~~~~python
a_dict={"name":"Tyrion", "height":1.35, 0:['imp','halfman']}
a_dict[("location","marriage")]=("Meereen","Married")  # add item
print a_dict[0]                                        # get item by key 0
a_dict["height"] = 1.85                                # modify item

print a_dict.keys()
print a_dict.values()
print a_dict.items()
print "name" in a_dict                                 # check in keys

# check help(dict)
print a_dict.get("wife","Sansa")                       # default value when key do not exist
print a_dict.pop(0,"Giant of Lannister")               # default value when key do not exist
~~~~

  ​

### 7. Str

#### a. Bit and Byte
- Bit is the smallest memory unit in computer, binary values 0 or 1
- Byte consists of 8 bits

~~~~python
# however, everything is an object in python, there would be extra cost, you'll understand in the "class" class
import sys
li = [0,1.0,"1","1s",[],{},()]
for item in li:
	print "{}, type {} has the size of {} bytes".format(item,type(item),sys.getsizeof(item))
print sys.maxint        # the actual number part is 4 bytes, 32 bits (may depend on machine)
print 2**(4*8-1)        # the first bit is for the positive/negetive sign
print bin(sys.maxint)   # bin() check the binary code of int
~~~~
- [Bitwise operations](https://wiki.python.org/moin/BitwiseOperators) in Python

~~~~python
# encoding=utf8
# 如果不在第一/二行声明编码，这个程序是无法运行的
# you only need to know about the existance of these operations
# bitwise operations are always very fast
print "binary of 9: ",bin(9)
print "binary of 7: ",bin(7)
print "binary of 6: ",bin(6),'\n'

print "6 & 9 is:",6 & 9             
print "bin(6&9):",bin(6&9),'\n'   # And, output bit is 1 only if two 1s are given
print "6 | 9 is:",6 | 9
print "bin(6|9):",bin(6|9),'\n'   # Or, output bit is 0 only if two 0s are given
print "6 ^ 7 is:",6 ^ 7
print "bin(6^7):",bin(6^7),'\n'   # Xor, output bit is 1 only when input with one 1 and one 0 
print "9 >> 2 is:",9>>2
print "bin(9>>2):",bin(9>>2),'\n' # x>>y, x give away last y bits, equal to divided by 2**y
print "9 << 1 is:",9<<1
print "bin(9>>1):",bin(9<<1),'\n' # x<<y, insert y bits to the tail of x, equal to time 2**y
print "~1   is:",~1
print "bin(~1):",bin(~1),'\n'     # ~x would be -x-1
# this involves awareness of complement (补码), I may tell you about it when we are both in good mood...
~~~~

  ​

#### b. ASCII
- [American Standard Code for Information Interchange](https://en.wikipedia.org/wiki/ASCII): they invented computer!
- Default format for **touch** and sublime text, verify by **file** command.
- Only 128 characters, 0-9, a-z, A-Z and some special ones

~~~~python
# chr(num) turn a num to character, ord(chr) takes back
for i in range(128):
	character=chr(i)
	print ord(character) ," : ",character
~~~~

  ​

#### c. String in Python
- Immutable object! Iterable.
- Escape characters (转义字符) (Common in languages)

~~~~python
print "He's a boy"=='He\'s a boy'   # "" same as '', use \ to escape
print "\\n is a new line: \n\\t is table: \t, \\a is bell: \a"
print r"\\n is a new line: \n\\t is table: \t, \\a is bell: \a"
# add 'r' refore string to supress special characters
print """(line1)There is a \\n here:
(line2)Can be supressed by \\: \
(line2)Can also work with raw"""
# rarely used, don't get confused with comments
~~~~
- Some of the important functions:

~~~~python
# String format
string="Salut, %s, here are %d dollars for you."%("amigo",100000)
print string
print "%%s for str:%s"%("String")     # here use % to escape
print "%%d for int: %d"%(1000)
print "%%.nf keep n digits for float: %.3f\n"%(3.14159)

print "{} is better than {}".format("HIMYM","Friends")
print ":n.nf keep n digits for float: {:2.3f}".format(3.14159)
string="'{1}' is '{0}' in orc language".format("Victory or death","Lok'tar ogar") 
print string,'\n\n'                   # format is very powerful, can not be all introduced

# Useful functions
poem="Let me feel the world as your love taking form\n"
print "Id before +=: ",id(poem)
poem+="Then my love will help it\nby Tagore\n"
print "Id after  +=: ",id(poem)       # a new str is returned
print poem,"\n"                       

print poem.split("\n"),'\n'           # result is a list
print poem.lower()                    # to lower case
print poem.lower().count("t"),"\n"
print poem.replace("love","cute"),"\n"
print "/".join(["1","14","2017"])     # very handy, take a list of strings

# many objects can be stringfied, by obj.__str__() function
print str(range(10))
print str(tuple(range(10)))
print str(dict(zip(range(10),range(10,20))))
# zip() is a useful function combining two list into one

# slicing, similar as list, ALWAYS return a new string (immutable!)
string="My alphabet starts where your alphabet ends."
print "Length of string:",len(string)
print string[3:11]
print string[3:-3]
# string[0]='0' this is illegal
~~~~
- [Documents](https://docs.python.org/2/library/string.html), or **help(str)**, practice is the only way to nail this...

#### d. Unicode and utf8 (Optional)
- This .md document contains 汉字, try **file** this file in shell
- Charset: complement ASCII

- 130 is "é" in French charset, but Gimel "ג" in Hebrew charset
- [Unicode](https://docs.python.org/2/howto/unicode.html): for all languages

  - 1,000,000+ characters
  - each character consists of 2 bytes, even for English characters
  - a binary mapped to a character, "严" is 100111000100101 (\x4e25)
  - no requirement on the storage method 

-   Utf8: all languages, condensed storage, most commonly used

    - 1-6 bytes, English characters 1 byte, most Chinese 3 bytes, more for rear ones
    - ASCII is the subset of Utf8, no legacy problems
    - [a good tutorial](http://www.ruanyifeng.com/blog/2007/10/ascii_unicode_and_utf-8.html)
-   In memory: all in Unicode; when stored/conveyed: charset like Utf8
-   Charsets are usually declared in the beginning of file/webpage

~~~~python
# -*- coding: utf-8 -*-
import sys

unicode_chine=u"瓷国"
print type(unicode_chine)
print len(unicode_chine)
print unicode_chine.__repr__()       # __repr__(): canonical string representation of the object, that is the binary representation
print unicode_chine,'\n'

utf8_chine="瓷国"
print type(utf8_chine)
print len(utf8_chine)
print utf8_chine.__repr__()          # e7 93 b7 e5 9b bd
print utf8_chine,'\n'                # windows uses gbk charset

decoded=utf8_chine.decode("utf8")    # decode from charset to unicode
print decoded==unicode_chine
encoded=unicode_chine.encode("utf8") # encode from unicode to charset
print encoded==utf8_chine

print decoded.encode("gbk")          # output platform has a charset
~~~~

- Gold suggestion: early decode, late encode, Unicode for internal processing 

#### e. Homework
- Print out all 26 Caesar ciphers for "Lancelot du Lac"

### 7.5. Addition
- If else
- None
- List comprehensions
- A [syntax sugar](https://en.wikipedia.org/wiki/Syntactic_sugar)

~~~python
li = range(10)
print [x*x for x in li]

def say(name): print name, " is already dead ~_~"
girls=["Shae","Margaery","Ygritte","Catelyn"]
print [say(n) for n in girls]
print [say(n) for n in girls if n!="Margaery"]  # with if filter

import os
print [x for x in os.listdir(".") if x.endswith("py")]

print [m+n for m in "ABC" for n in "abc"]       #double iterations

d = {'x': 'A', 'y': 'B', 'z': 'C' }             # ditto in dict
print {m:n for m,n in d.items()}                # exchange key/values
~~~
