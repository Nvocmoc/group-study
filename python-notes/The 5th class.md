# Class
## 1. Complement
- **None** object
- Can do almost nothing, but is an object, try **help(None)**

~~~~python
# some well-used functions may modify itself but return a None
li = range(10)
dummy = li.reverse()             # reverse list
print li
print dummy,'\n'
dummy = li.sort()                # sort list
print li
print dummy,"\n"
dummy = li.append(0)
print sorted(li)
print dummy,'\n'

if None:
	print "This is None"
elif not None:                 # elif is short for else if
	print "None is like False"
else:
  print "ALl false"

# print True & None  # can be judged, still can't be treated as bool
~~~~

  ​
- List comprehensions
- A [syntax sugar](https://en.wikipedia.org/wiki/Syntactic_sugar)

~~~~python
# [func_return(args) for arg1 in list1 for arg2 in list2... if func_return_bool(agrs)] same as:
# li=[]
# for arg1 in list1:
#    for arg2 in list2:
#       if func_return_bool(agrs):
#             li.append(func_return(args))
print [x*x for x in range(10)],"\n"
li = []
for x in range(10):
  li.append(x*x)
print li

from math import pi                                 # special import
print [round(pi,i) for i in range(6)],"\n"

def say(name): print name, " is already dead ~_~"
girls=["Shae","Margaery","Ygritte","Catelyn"]
print [say(n) for n in girls if n!="Margaery"],"\n" # with judgements

print [m+n for m in "ABC" for n in "abc"],"\n"      # double iterations

import os as mac
generator = (x for x in mac.listdir(".") if x.endswith("py"))
print hex(id(generator))
print generator
print list(generator)                               # constructed by iterable
print tuple("Death Wing"),"\n"                      # ditto in tuples & sets

d = {'x': 'A', 'y': 'B', 'z': 'C' }                 # ditto in dict
print {m:n for m,n in d.items()}                    # swap key/values
~~~~

## 2. Class in Python

### a. OOP
- Object-Oriented Programming 
- Deep philosophies, this one is mainly for you to better understand Python mechanisms
- Also exist Process-Oriented and Functional Programming
- An abstract way to divide problems into objects and interaction between objects, common in large programs
    - Java for Android
    - Object-C for IOS
- Everything is an object in Python -- int, str, list, even functions and libraries/modules
- Object - can be assigned to a variable, passed into a function, returned by function
- Three features: ***encapsulation***, ***inheritance***, ***polymorphism***

### b. Definition
- Capital class name by convention
- Consists of variables(static/normal), methods(normal/class/static), property, only introduce first three
- Entrance: \_\_init\_\_(), can be left blank
- Exit: \_\_del\_\_() (Do not care because of existence of **Garbage Collection**) 

~~~~python
class Stark(object):
	"""This is for Stark wolves	"""
	motto="Hear me ro.. eh... winter is coming!"  # static variable, belong to class
	def __init__(self,arg_name,arg_age): 
		"""Describing __init__ function"""
		self.name=arg_name                        # normal variable, belong to instance
		self.age=arg_age
		print self.name,"of house Stark is born at age",arg_age
    
	def grow(self):            
     # first arg is always "self", address info, automatically filled in
		"""Describing grow function"""
		self.age+=1
		print self.name,"getting old, now ",self.age,'\n'
      
print "dir(Stark): ",dir(Stark)
print Stark.__dict__           # contain all the things you can do with it
help(Stark)       
# when try a.x, look into a.__dict__["x"], then type(a).__dict__["x"]

Sansa=Stark("Sansa",11)        # create an instance by calling __init__
Bran=Stark("Bran",8)

print "\nStark: ",Stark        # Stark is the class
print Sansa                    # Sansa is a instance of Class
print "Sansa.age: ",Sansa.age  # use dot to get attributes
print Bran
Bran.grow()

print "dir(Bran): ",dir(Bran),"\n"
print "dir(Bran.grow): ",dir(Bran.grow),"\n\n\n\n\n"

import inspect 
from pprint import pprint 
# inspect.getmembers(obj) get most info about the obj
print "\nStark:"
pprint(inspect.getmembers(Stark))
print "\nBran:"
pprint(inspect.getmembers(Bran))
~~~~
- Try create another script in the same folder and import and dir this script
- Try wrap everything below class definition inside "if \_\_name\_\_==\_\_main\_\_:"

### c. Encapsulation (Optional)
- Attributes can be public or private

~~~~python
class StarkKid(object):
	_location="Winterfell"    # single underline, saying, you can change me, but please don't
	def __init__(self,arg_name): 
		self.name=arg_name    # normal contribution
		self.__age=0          # double underlines, only achieved inside class
	def speak(self):
	    print self.name,"is",self.__age,"now, at",self._location
	def setAge(self,age):
		if age>10:
			print "Tooo old to be a kid, let's say",self.name,"is 3."
			age=3
		self.__age=age       # modifying private variable

Arya=StarkKid("Arya") 
# print Arya.__age          
Arya.setAge(8)
Arya.speak()
print Arya._StarkKid__age     # here is the trick, not guaranteed

Ned=StarkKid("Ned")           
Ned.setAge(35)                # don't allow people to mess up
Ned.speak()

StarkKid._location="King's landing"
Arya.speak()                 
Ned.speak()                   # changing all kids
print "\nWhere is _location:"
print id(Arya._location)
print id(Ned._location)
print id(StarkKid._location)  # _location belong to the class
# print StarkKid.name         # name belong to instances
# There would be only one _location in memory, but many name variables

Arya._location="Braavos"
print "\nAfter changing:"
print id(Arya._location)
print id(Ned._location)
print id(StarkKid._location)
~~~~

  ​

### d. Inheritance  and Polymorphism (Optional)
- Subclass inherit every function/attribute from Base/Super classes, if blank, from **Object** class
- In Python, can inherit from more than one classes
- Introduction of namespace

~~~~python
class NorthFamily(object):
	"""docstring for NorthFamily"""
	def __init__(self, name):
		self.name = name
	def speak(self):
		print "The north never forgets"

class Stark(NorthFamily):
	"""docstring for Stark"""
	def __init__(self, name):
		super(Stark, self).__init__(name)
	def speak(self):
		super(Stark, self).speak()
		print "Winter is coming"
	def skill(self):
		print "We have pet wolves"

class Bolton(NorthFamily):
	"""docstring for Bolton"""
	def __init__(self, name):
		super(Bolton, self).__init__(name) # super() calles the base method
		print "A Bolton is born: ",name

	def speak(self):
		print "Our Blades are Sharp"
	def skill(self):
		print "We flay the Starks"
		
Ned=Stark("Ned")
print isinstance(Ned,Stark)
print isinstance(Ned,NorthFamily)
Ned.speak()
Ned.skill()
print "\n"
def test(house):                            # the duck type
	house.skill()
test(Ned)
print "\n"
test(Bolton("Roose Bolton"))                # an anonymous instance
~~~~

### e. Built-in functions 
- Print/str(): \_\_str\_\_():

~~~~python
class Dragon(object):
	def __init__(self,name="Deathwing"):
		self.name=name
	def __str__(self):                       # only work on instances
		print "\nOur dragons only eat apple"
		return "Apple eater: "+self.name

print "Str(Dragon): ",Dragon
print "Str(Dragon()): ",Dragon()
print "Str(Dragon("Rhaegal")): ",Dragon("Rhaegal")
~~~~
- Iterable: \_\_iter\_\_() and next():

~~~~python
class MyList(list):                           # customized list, mostly identical to List
  def __iter__(self):
  	return self                           # return a iterable object, with next() method
  def next(self):
  	if self.__len__()==0:
  		raise StopIteration()            # for loop stop when StopIteration detected
      last=self.pop()                       # self.pop is same as list.pop
      return last

myli=MyList(range(10))
print myli
for i in myli:print i
~~~~

~~~~python
class Fibonacci(object):
	def __init__(self,max=1000):              # functions can always have default args
		self.a=0
		self.b=1
		self.max=max
	def __iter__(self):
		return self
	def next(self):
		self.a,self.b=self.b,self.a+self.b    # like magic, realized by tuple
		if self.b>self.max:
			raise StopIteration()
		return self.b

print list(Fibonacci())
print list(Fibonacci(10000))

dummy=1,2
print dummy
~~~~
- Slice and index: \_\_getitem\_\_()

~~~~python
class Weirdo(object):
	def __getitem__(self,index):             # index is what inside the braket
		if isinstance(index,int):          
			return index*index
		elif isinstance(index,slice):        # slice is also an object
			start=index.start
			stop=index.stop
			return start*start+stop*stop

we=Weirdo()
print we[10]
print we.__getitem__(10)
print we[3:4]
print we.__getitem__(slice(3,4))
# but if you want to modify elements by we[1]=10, there are __setitem__ and __delitem__ for you
~~~~

  ​
- Callable: \_\_call\_\_()

~~~~python
class Phone(object):
	def __call__(self,number="18888888888"):
		print "Calling: ",number
ph=Phone()
ph()
Phone()("10000000000")
print callable(ph)                           # tell you whether an object is callable
~~~~



