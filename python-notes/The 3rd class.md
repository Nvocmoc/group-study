#  The third class

## Built-in types

### 1. Boolean
### 2. Numbers
### 3. List
- Many languages would provide **Array**, faster but harder to operate, **list** is adorable
- Ordered, mutable, index start from 0
- Many functions, use **help()** or stack overflow
- Better with practices:
```python
# try help(list)
empty = []
li=[1, 1.0, '2017', range(10), True]
range(10)                # a very useful list generator, 0~9

print len(li)            # len() give the number of elements in list
print len(li[3])
print li[3][0]           # two dimensional arrays

# Ways to modify elements
li[0]=100 
li.append(100)           # append() add single item to its tail
li.extend(range(3))      # extend() sew another list to its tail, adding 3 elements
li.append(range(3))      # append() still add just 1 element
li+=[1]                  # same as li.extend([1])
li.insert(0,'blah') 

# Find elements in a list
alist=['a','b','a','c']
print alist.count("a")
print "a" in alist       # return a boolean
print alist.index("a")   # return first index, try index "b" and "e"

# Ways to delete elements
del li[0] 
li.remove(1.0)           # try twice

# List slice is the most attractive feature of list
print li[0]              # first element
# li[10] do not exist, would give you error
print li[-1]             # the last element, -2 would be second to last
print li[1:3]            # List[start:end] including start, excluding end
print li[:3]             # no left means iterate from the first element
print li[1:]             # no right means iterate to the last element
print li[:]              # a same, but new list
print li[::2]            # List[start:end:step] step is optional
# ALL slice operation would return a new list!

if not []: print "Empty list would be False"
if [False]: print "List with False as its value would be True"
```
- The trivial trap of copies (memory mechanism):

~~~~python
alist = range(10)
alist.append(range(5))
blist = alist
clist = alist[:]

def check(a,b,c):
	print "================= CHECKING ================="
	print "Contents of three lists:"
	print "a: ",a
	print "b: ",b
	print "c: ",c

	print "\nAddresses of three lists:"    
	print "a: ",id(a)
	print "b: ",id(b)
	print "c: ",id(c)

	print "\nAddresses of first elements:"
	print "a: ",id(a[0])
	print "b: ",id(b[0])
	print "c: ",id(c[0])

	print "\nAddresses of last elements:"
	print "a: ",id(a[-1])
	print "b: ",id(b[-1])
	print "c: ",id(c[-1])
	print "================= CHECKED =================\n\n\n"

print "==> When initialized: <=="
print "The id of 0 in the nested list: ",id(alist[-1][0])
check(alist,blist,clist)

print "==> Changing the immutable element: <=="
alist[0] = 200 
check(alist,blist,clist)      # list[:] would not be affected  

print "==> Changing the  mutable element: <=="
alist[-1][-1]=100
check(alist,blist,clist)      # list[:] would be affected

# I would draw a digram to explain this.

# copy.deepcopy() function would be only copy values
print "==> After using deepcopy: <=="
import copy 
clist=copy.deepcopy(alist)
check(alist,blist,clist)

print "==> Changing the last, mutable element: <=="
alist[-1][-1] = 1000
check(alist,blist,clist)
~~~~


### 4. Tuple
- Frozen List, ordered, immutable, and would be hierarchical:
     - list: [ student1, student2, student3, ...]
     - tuple: ( year, month, day, hour, minute )
- Faster speed, same index and slice operations
- Same old:
```python
tu=tuple(range(10))      # can be created by a type convert from List
# tu[0]=1 would be illegal
li=list(tu)              # vice versa

print 2 in tu            # True
print tu.index(3)

if not (): print "Empty tuple would be False"
if (False,): print "Tuple with False as its value would be True"

print type(())
print type((False))
print type((False,))
```
- Multi-variables function return: realized by a tuple
```python
a, b = 1, 2
b, a = a, b
print  a, b
def dummy(): return "CUSS", "HIM"
result = dummy()
print result

a,b = result           # automatically unpack, try a,b,c=dummy()
print a,b 
# Question: List has same feature, why didn't I introduce there?

print type(dummy())
print type(dummy)
```
- Only the address is immutable:
~~~~python
li = range(5)
li.append(range(5))
tu = tuple(li)
print tu
tu[-1][-1] = 100
print tu
~~~~


### 4.5 Loops
- Lists are very commonly used in for loops:

~~~~python
# -*- coding: utf-8 -*-
# for loops stop when the iterable object run out of items
for i in ["Shae","Margaery","Ygritte","Catelyn"]: 
  print i," is already dead ~_~"

summation = 0
for num in range(100):                      # range(100) is 0 to 99!    
  summation += num
print "summation from 0 to 99: ",summation 

# try interpret this:
for item in [("China","Chine"),("Amecria","Amécria"),("France","France")]:
  x,y=item
  print x,y
~~~~
- Another kind of while loop:

~~~~python
# while loops stop when the boolean judgement become False
i = 0                             # usually a variable is used to control the loop
summation = []
while i < 10:                     # the situation of i=10 would not run
  summation.append(i)
  i+=1                          # what would happen if we do not modify i?
print summation
~~~~
- Two keywords: **break** and **continue**：

~~~~python
# in while loop
large=100
small=10
while True:       # if a "while True" is used, the body must have a break!
  if large < small: 
      break
  large-=3
  small+=2
print large, small

# in for loop
for i in ["Shae","Margaery","Ygritte","Catelyn"]: 
  if i=="Margaery":
      continue
  print i," is already dead ~_~"
~~~~
- Homework: print all prime numbers in range(1001)


### 5. Set

### 6. Dictionary

### 7. String